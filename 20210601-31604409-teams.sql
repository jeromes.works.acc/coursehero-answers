CREATE TABLE `coaches` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `home_phone_number` varchar(20) NOT NULL,
  PRIMARY KEY (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `parents` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `home_phone_number` varchar(20) NOT NULL,
  `street` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(3) NOT NULL,
  `zip` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `players` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `age` smallint(6) NOT NULL,
  PRIMARY KEY (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `teams` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) NOT NULL,
  `colors` varchar(50) NOT NULL,
  PRIMARY KEY (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `player_parents` (
  `player_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  CONSTRAINT `player_parents_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `parents` (`id_number`),
  CONSTRAINT `player_parents_ibfk_2` FOREIGN KEY (`player_id`) REFERENCES `players` (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `team_coaches` (
  `team_id` int(11) NOT NULL,
  `coach_id` int(11) NOT NULL,
  CONSTRAINT `team_coaches_ibfk_1` FOREIGN KEY (`coach_id`) REFERENCES `coaches` (`id_number`),
  CONSTRAINT `team_coaches_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `team_players` (
  `team_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  CONSTRAINT `team_players_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id_number`),
  CONSTRAINT `team_players_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
