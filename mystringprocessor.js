function myStringProcessor(inputString) {
  var pattern = /(^[A-Z]|\*$|\!$)/;
  var matchedStrings = [];
  var splitStrings = inputString.split(",");
  splitStrings.forEach(function(item,index){
  	trimmedString = item.trim();
    if (pattern.test(trimmedString)) {
        console.log(trimmedString);
    	matchedStrings.push(trimmedString);
    }
  });
  return matchedStrings.join("_").toUpperCase();
}
