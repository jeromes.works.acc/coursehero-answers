-- PCORnet CDM queries

-- 4) Starter Code modification to get only the POSITIVE test results and count the unique patients
SELECT COUNT(DISTINCT(PATID)) FROM HUSH_LAB_RESULT_CM WHERE LAB_LOINC = '85476-0' AND RAW_RESULT LIKE "%POSITIVE%";

-- 8) top diagnostic codes
SELECT LAB_PX_TYPE, COUNT(*) FROM HUSH_LAB_RESULT_CM WHERE LAB_PX_TYPE = "10" GROUP BY LAB_PX_TYPE;

-- 10) demographics
-- columns :  HISPANIC  |  RACE  |  MALE  |  FEMALE
SELECT hispanic, race, [m], [f] FROM
(
    SELECT hispanic, race, sex as [birhtSex] FROM hush_demographic WHERE sex IN ('M', 'F')
) AS src
PIVOT 
(
    count(birthSex)
    FOR [birthSex] IN ([M], [F])
) AS pvt
