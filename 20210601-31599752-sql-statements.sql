-- NAME: ________; Student ID: ________; Date: _________; Purpose: ________

SET AUTOCOMMIT ON;

-- for #1, please make sure that the employee table name and the column names are correct
INSERT INTO employee (employee_number, email_address, job_title, office_code, superior_employee_id)
VALUES (31599752, "your.email@email.com", "Cashier", 4, 1088);

-- for #2, please make sure that the employee table name and the column names are correct
SELECT * FROM employee WHERE employee_number = 31599752;

-- for #3, please make sure that the employee table name and the column names are correct
UPDATE employee SET job_title="Head Cashier" WHERE employee_number = 31599752;

-- for #4, again, please make sure that the employee table name is correct
INSERT INTO employee (employee_number, email_address, job_title, office_code, superior_employee_id)
VALUES (999999, "fake.employee@email.com", "Cashier", 4, 31599752);

-- for #5, this will not work because the "superior_employee_id" is a foreign key
DELETE FROM emplyee WHERE employee_number = 31599752;

-- for #6, this will now work because there is no other referencing the foreign key
DELETE FROM employee WHERE employee_number = 999999;
DELETE FROM employee WHERE employee_number = 31599752;

-- for #7
INSERT INTO employee (employee_number, email_address, job_title, office_code, superior_employee_id)
VALUES 
(31599752, "your.email@email.com", "Cashier", 4, 1088),
(999999, "fake.employee@email.com", "Cashier", 4, 1088);

-- for #8
DELETE FROM employee WHERE employee_number IN (31599752, 999999);
